//
//  HKCircuralProgressViewWithButtons.h
//  HKCircularProgressView
//
//  Created by Kamil Woźniak on 30.05.2014.
//  Copyright (c) 2014 Panos Baroudjian. All rights reserved.
//

#import "HKCircularProgressView.h"

@interface HKCircuralProgressViewWithButtons : HKCircularProgressView

@property (nonatomic, strong) NSMutableArray *buttonsArray;

-(CGPoint)getPointForValue:(CGFloat)_value andOffset:(CGFloat)_offset andButtonWidth:(CGFloat)_width andButtonHeight:(CGFloat)_height;
-(void)setPositionForButton:(UIButton*)_button andValue:(CGFloat)_value andOffset:(CGFloat)_offset andWidth:(CGFloat)_width andHeight:(CGFloat)_height;

@end
