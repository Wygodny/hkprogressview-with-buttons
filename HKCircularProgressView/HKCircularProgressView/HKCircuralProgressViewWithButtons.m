//
//  HKCircuralProgressViewWithButtons.m
//  HKCircularProgressView
//
//  Created by Kamil Woźniak on 30.05.2014.
//  Copyright (c) 2014 Panos Baroudjian. All rights reserved.
//

#import "HKCircuralProgressViewWithButtons.h"

@implementation HKCircuralProgressViewWithButtons
@synthesize buttonsArray;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        buttonsArray = [NSMutableArray array];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(CGPoint)getPointForValue:(CGFloat)_value andOffset:(CGFloat)_offset andButtonWidth:(CGFloat)_width andButtonHeight:(CGFloat)_height
{
    CGPoint center = CGPointMake(self.layer.bounds.size.width * .5f, self.layer.bounds.size.height * .5f);
    CGFloat radius = (MIN(self.layer.bounds.size.width, self.layer.bounds.size.height) * .5f) + _offset;
    CGFloat angle = 360 * _value/self.max;
    angle = angle * M_PI / 180;
    
    CGPoint point;
    
    point.x = radius * cos(angle) + center.x - _width/2;
    point.y = radius * sin(angle) + center.y - _height/2;
    
    return point;
}

-(void)setPositionForButton:(UIButton *)_button andValue:(CGFloat)_value andOffset:(CGFloat)_offset andWidth:(CGFloat)_width andHeight:(CGFloat)_height
{
    CGFloat angle = 360 * _value/self.max;    
    CGPoint point = [self getPointForValue:_value andOffset:_offset andButtonWidth:_width andButtonHeight:_height];
    _button.frame = CGRectMake(point.x, point.y, _width, _height);
    _button.transform = CGAffineTransformMakeRotation((angle + 180)*M_PI/180.0);
}

@end
